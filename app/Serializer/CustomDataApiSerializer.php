<?php namespace App\Serializer;

use League\Fractal\Pagination\PaginatorInterface;
use League\Fractal\Serializer\ArraySerializer;

class CustomDataApiSerializer extends ArraySerializer {
    public function paginator(PaginatorInterface $paginator)
    {
        $meta = parent::paginator($paginator);


        // here your would manipulate the pagination data
        if( empty($meta["pagination"]["links"]) ){
            $meta["pagination"]["links"] = [
                "next" => null
            ];
        }

        return $meta;
    }
}