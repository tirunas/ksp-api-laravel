<?php

namespace App;

use App\Constants\RolesConstants;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const DB_TABLE = 'roles';

    const PRIMARY_KEY = 'id';
    const A_NAME = 'name';
    const A_READABLE_NAME = 'readable_name';

    const FK_ROLE_ID = 'role_id';

    public function getId(): int {
        return $this->getAttribute(self::PRIMARY_KEY);
    }

    public function setId(int $value)
    {
        $this->setAttribute(self::PRIMARY_KEY, $value);
    }

    public function getName(): string {
        return $this->getAttribute(self::A_NAME);
    }

    public function setName(string $value)
    {
        $this->setAttribute(self::A_NAME, $value);
    }

    public function getReadableName(): string {
        return $this->getAttribute(self::A_READABLE_NAME);
    }

    public function setReadableName(string $value)
    {
        $this->setAttribute(self::A_READABLE_NAME, $value);
    }

    public function getPermissions(): array
    {
        return array_get(RolesConstants::ROLES_PERMISSIONS, $this->getName());
    }
}
