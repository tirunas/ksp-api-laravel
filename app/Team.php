<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    const PK = 'id';
    const A_NAME = 'name';
    const A_ARENA = 'arena';
    const A_ADDRESS = 'address';

    const REL_USERS = 'users';

    const FK_TEAM_ID = 'team_id';

    protected $fillable = [
        self::A_NAME,
        self::A_ARENA,
        self::A_ADDRESS
    ];


    public function getId(): int {
        return $this->getAttribute(self::PK);
    }

    public function setId(int $value)
    {
        $this->setAttribute(self::PK, $value);
    }

    public function getName(): string {
        return $this->getAttribute(self::A_NAME);
    }

    public function setName(string $value)
    {
        $this->setAttribute(self::A_NAME, $value);
    }

    public function getArena(): ?string {
        return $this->getAttribute(self::A_ARENA);
    }

    public function setArena(string $value)
    {
        $this->setAttribute(self::A_ARENA, $value);
    }

    public function getAddress(): ?string {
        return $this->getAttribute(self::A_ADDRESS);
    }

    public function setAddress(string $value)
    {
        $this->setAttribute(self::A_ADDRESS, $value);
    }

    public function getUsers()
    {
        $this->getRelation(self::REL_USERS);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'team_user', self::FK_TEAM_ID, User::FK_USER_ID);
    }
}
