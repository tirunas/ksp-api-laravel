<?php namespace App\Http\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract {

    protected $availableIncludes = [
        'teams',
        'roles'
    ];

    protected $defaultIncludes = [

    ];

    public function transform(User $user)
    {
        return [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'surname' => $user->getSurname(),
            'email' => $user->getEmail(),
            'city' => $user->getCity(),
            'birthday' => $user->getBirthday(),
            'status' => $user->getStatus(),
            'referee_exp' => $user->getRefereeExp()
        ];
    }

    public function includeRoles(User $user)
    {
        $roles = $user->getRoles();

        return $this->collection($roles, new RoleTransformer());
    }

    public function includeTeams(User $user)
    {
        $teams = $user->getTeams();
        return $this->collection($teams, new TeamTransformer());
    }
}