<?php namespace App\Http\Transformers;

use App\Role;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract {
    protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform(Role $role)
    {
        return [
            'id' => $role->getId(),
            'name' => $role->getName(),
            'readable_name' => $role->getReadableName(),
            'permissions' => $role->getPermissions()
        ];
    }
}