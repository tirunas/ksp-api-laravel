<?php namespace App\Http\Transformers;

use App\Team;
use League\Fractal\TransformerAbstract;

class TeamTransformer extends TransformerAbstract {

    protected $availableIncludes = [
        'users'
    ];

    protected $defaultIncludes = [

    ];

    public function transform(Team $team)
    {
        return [
            'id' => $team->getId(),
            'name' => $team->getName(),
            'arena' => $team->getArena(),
            'address' => $team->getAddress()
        ];
    }

    public function includeUsers(Team $team)
    {
        $users = $team->getUsers();

        return $this->collection($users, new UserTransformer());
    }
}