<?php namespace App\Http\Responses;

class Response {
    public static function error($message, int $code = 400) {
        return response()->json($message, $code);
    }

    public static function created($message, int $code = 201) {
        return response()->json($message, $code);
    }

    public static function ok($message, int $code = 200) {
        return response()->json($message, $code);
    }

    public static function noContent() {
        return response()->json(null, 204);
    }

    public static function notFound($message) {
        return response()->json($message, 404);
    }

    public static function unauthorized() {
        return response()->json('Unauthorized', 401);
    }
}