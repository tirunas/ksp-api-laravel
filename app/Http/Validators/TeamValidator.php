<?php namespace App\Http\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class TeamValidator extends LaravelValidator {
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|unique:teams',
        ],
        ValidatorInterface::RULE_UPDATE => [

        ]
    ];
}