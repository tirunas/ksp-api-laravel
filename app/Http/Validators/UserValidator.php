<?php namespace App\Http\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class UserValidator extends LaravelValidator {
    const RULE_REGISTER = 'register';
    const RULE_LOGIN = 'login';

    const MIN_PASS_LENGTH = 6;


    const NAME = 'name';
    const SURNAME = 'surname';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const REPEAT_PASSWORD = 'repeat_password';

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            self::NAME => 'required',
            self::SURNAME => 'required',
            self::EMAIL => 'required|unique:users',
        ],
        ValidatorInterface::RULE_UPDATE => [

        ],
        UserValidator::RULE_REGISTER => [
            self::NAME => 'required|string',
            self::SURNAME => 'required|string',
            self::EMAIL => 'required|email|unique:users',
            self::PASSWORD => 'required|string|min:'.self::MIN_PASS_LENGTH,
            self::REPEAT_PASSWORD => 'required|string|min:'.self::MIN_PASS_LENGTH.'|same:'.self::PASSWORD
        ],
        UserValidator::RULE_LOGIN => [
            self::EMAIL => 'required|email',
            self::PASSWORD => 'required|min:'.self::MIN_PASS_LENGTH
        ]
    ];

    protected $messages = [
//        'email.required' => 'We need to know your email address'
    ];
}