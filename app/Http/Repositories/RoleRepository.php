<?php namespace App\Http\Repositories;

use App\Role;
use Prettus\Repository\Eloquent\BaseRepository;

class RoleRepository extends BaseRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Role::class;
    }
}