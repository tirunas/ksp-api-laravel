<?php namespace App\Http\Repositories;

use App\Http\Validators\TeamValidator;
use App\Team;
use Prettus\Repository\Eloquent\BaseRepository;

class TeamRepository extends BaseRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Team::class;
    }

    public function validator()
    {
        return TeamValidator::class;
    }
}