<?php

namespace App\Http\Controllers;

use App\Http\Repositories\TeamRepository;
use App\Http\Transformers\TeamTransformer;
use App\Http\Validators\TeamValidator;
use Illuminate\Http\Request;
use League\Fractal\Manager;

class TeamController extends Controller
{
    /**
     * UserController constructor.
     * @param Manager $fractal
     * @param Request $request
     * @param TeamRepository $repository
     * @param TeamTransformer $transformer
     * @param TeamValidator $validator
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __construct(Manager $fractal, Request $request, TeamRepository $repository, TeamTransformer $transformer, TeamValidator $validator)
    {
        parent::__construct($fractal, $request, $repository, $transformer, $validator);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->paginatedAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return $this->createItem();
    }

    public function show($id)
    {
        return $this->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        return $this->updateItem($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->delete($id);
    }
}
