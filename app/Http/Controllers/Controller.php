<?php

namespace App\Http\Controllers;

use App\Http\Criterias\IncludesCriteria;
use App\Http\Responses\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Exceptions\ValidatorException;
use Prettus\Validator\LaravelValidator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const REQ_INCLUDES = 'includes';
    const REQ_LIMIT = 'limit';

    /**
     * @var Manager $fractal
     */
    protected $fractal;
    /**
     * @var BaseRepository $repository
     */
    protected $repository;
    /**
     * @var TransformerAbstract $transformer
     */
    protected $transformer;
    /**
     * @var Request $request
     */
    protected $request;
    /**
     * @var string|array|null $includes
     */
    protected $includes;
    protected $limit;

    /**
     * Controller constructor.
     * @param Manager $fractal
     * @param Request $request
     * @param BaseRepository $repository
     * @param TransformerAbstract $transformer
     * @param LaravelValidator $validator
     * @throws RepositoryException
     */
    public function __construct(Manager $fractal, Request $request, BaseRepository $repository, TransformerAbstract $transformer, LaravelValidator $validator)
    {
        $this->fractal = $fractal;
        $this->request = $request;
        $this->transformer = $transformer;
        $this->validator = $validator;
        $this->includes = $request->get(self::REQ_INCLUDES);
        $this->limit = $request->get(self::REQ_LIMIT);
        try {
            $this->repository = $repository->pushCriteria(new RequestCriteria($this->request));
        } catch (RepositoryException $e) {
            $this->repository = $repository;
        }
        if ($this->includes) {
            $this->repository = $this->repository->pushCriteria(new IncludesCriteria($this->includes));
        }
    }

    public function createItem()
    {
        try {
            $user = $this->repository->create($this->request->all());
        } catch (ValidatorException $e) {
            return Response::error($e);
        }
        return Response::created($this->itemTransform($user, $this->transformer));
    }

    public function updateItem(int $id) {
        try {
            try {
                $user = $this->repository->update($this->request->all(), $id);
            } catch (ModelNotFoundException $e) {
                return Response::notFound(['message' => $e->getMessage()]);
            }
        } catch (ValidatorException $e) {
            return Response::error($e);
        }
        return Response::ok($this->itemTransform($user, $this->transformer));
    }

    public function delete(int $id) {
        try {
            $this->repository->delete($id);
        } catch (ModelNotFoundException $e) {
            return Response::notFound(['message' => $e->getMessage()]);
        }
        Response::noContent();
    }

    protected function all(array $columns = null) {
        return Response::ok($this->collectionTransform($this->repository->all($columns), $this->transformer));
    }

    protected function find(int $id, array $columns = null) {
        try {
            $user = $this->repository->find($id, $columns);
        } catch (ModelNotFoundException $e) {
            return Response::notFound(['message' => $e->getMessage()]);
        }
        return Response::ok($this->itemTransform($user, $this->transformer));
    }

    protected function validated($rule) {
        try {
            $this->validator
                ->with($this->request->all())
                ->passesOrFail($rule);
        } catch (ValidatorException $exception) {
            return $exception;
        }
        return true;
    }

    /**
     * Get param from request
     * @param string $name
     * @return mixed
     */
    protected function getParam(string $name) {
        return $this->request->get($name);
    }

    protected function paginatedAll(array $columns = null) {
        $data = $this->repository->paginate($this->limit, $columns);

        return Response::ok($this->paginatedCollectionTransform($data, $this->transformer));
    }

    protected function paginatedCollectionTransform($data, $transformer): array {
        $this->assertIncludes();
        $collection = new Collection($data, $transformer);
        $collection->setPaginator(new IlluminatePaginatorAdapter($data));
        $collection = $this->fractal->createData($collection);

        return $collection->toArray();
    }

    protected function collectionTransform($data, $transformer): array {
        $this->assertIncludes();
        $collection = new Collection($data, $transformer);
        $collection = $this->fractal->createData($collection);

        return $collection->toArray();
    }

    protected function itemTransform($data, $transformer): array {
        $this->assertIncludes();
        $item = new Item($data, $transformer);
        $item = $this->fractal->createData($item);

        return $item->toArray();
    }

    private function assertIncludes() {
        if ($this->includes) {
            $this->fractal->parseIncludes($this->includes);
        }
    }
}
