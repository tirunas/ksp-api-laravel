<?php

namespace App\Http\Controllers;

use App\Constants\RolesConstants;
use App\Http\Responses\Response;
use App\Http\Validators\UserValidator;
use App\Role;
use App\User;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Http\Repositories\UserRepository;
use App\Http\Transformers\UserTransformer;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class UserController extends Controller
{
    /**
     * UserController constructor.
     * @param Manager $fractal
     * @param Request $request
     * @param UserRepository $repository
     * @param UserTransformer $transformer
     * @param UserValidator $validator
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __construct(Manager $fractal, Request $request, UserRepository $repository, UserTransformer $transformer, UserValidator $validator)
    {
        parent::__construct($fractal, $request, $repository, $transformer, $validator);
    }

    /**
     * @return array
     */
    public function index()
    {
        return $this->paginatedAll();
    }

    public function store()
    {
        /** @var User $user */
        $user = new User();
        try {
            $this->validator
                ->with($this->request->all())
                ->passesOrFail(ValidatorInterface::RULE_CREATE);
        } catch (ValidatorException $exception) {
            return Response::error($exception);
        }

        $user->fill($this->request->all());

        $user->save();

        /** @var Role $userRole */
        $userRole = Role::where(Role::A_NAME, RolesConstants::R_USER)
            ->first();

        $user->roles()->attach($userRole->getId());

        return $this->find($user->getId());
    }

    public function show($id)
    {
        return $this->find($id);
    }

    public function update($id)
    {
        return $this->updateItem($id);
    }

    public function destroy($id)
    {
        return $this->delete($id);
    }
}
