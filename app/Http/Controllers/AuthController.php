<?php

namespace App\Http\Controllers;

use App\Http\Repositories\UserRepository;
use App\Http\Responses\Response;
use App\Http\Transformers\UserTransformer;
use App\Http\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * UserController constructor.
     * @param Manager $fractal
     * @param Request $request
     * @param UserRepository $repository
     * @param UserTransformer $transformer
     * @param UserValidator $validator
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __construct(Manager $fractal, Request $request, UserRepository $repository, UserTransformer $transformer, UserValidator $validator)
    {
        parent::__construct($fractal, $request, $repository, $transformer, $validator);
    }

    /**
     * User login method
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $validation = $this->validated(UserValidator::RULE_LOGIN);
        if ($validation !== true) {
            return Response::error($validation);
        }

        $credentials = $this->request->only(UserValidator::EMAIL, UserValidator::PASSWORD);

        if (! $token = JWTAuth::attempt($credentials)) {
            return Response::unauthorized();
        }

        return Response::ok([
            'token' => $token,
            'user' => $this->itemTransform(Auth::user(), $this->transformer)
        ]);
    }

    /**
     * Register to system
     * @return \Illuminate\Http\JsonResponse
     */
    public function register()
    {
        $validation = $this->validated(UserValidator::RULE_REGISTER);
        if ($validation !== true) {
            return Response::error($validation);
        }

        /** @var User $user */
        $user = new User();

        $user->setName($this->getParam(UserValidator::NAME));
        $user->setSurname($this->getParam(UserValidator::SURNAME));
        $user->setEmail($this->getParam(UserValidator::EMAIL));
        $user->setPassword(
            bcrypt($this->getParam(UserValidator::PASSWORD))
        );

        $user->save();

        return Response::created($this->itemTransform($user, $this->transformer));
    }

    public function logout()
    {
        JWTAuth::invalidate();

        return Response::ok(true);
    }

    public function refresh()
    {
        $token = JWTAuth::getToken();
        if ( ! $token){
            return Response::error(['error' => 'Invalid token']);
        }
        try {
            $newToken = JWTAuth::refresh($token);
        } catch(TokenInvalidException $e) {
            return Response::error(['error' => 'Invalid token']);
        }
        return Response::ok(['token' => $newToken]);
    }
}
