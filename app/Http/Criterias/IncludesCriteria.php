<?php namespace App\Http\Criterias;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class IncludesCriteria implements CriteriaInterface
{

    protected $includes;

    public function __construct($includes)
    {
        $this->includes = explode(',', $includes);
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->with($this->includes);
    }
}