<?php

namespace App\Console\Commands;

use App\Constants\RolesConstants;
use App\Role;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Output\ConsoleOutput;

class RegenerateRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'regenerate:roles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command regenerate roles list in db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Role::truncate();
        (new \Symfony\Component\Console\Output\ConsoleOutput)->writeLn('Truncated all table data');
        foreach (RolesConstants::ROLES_READABLE_NAMES as $role => $readableName) {
            DB::table(Role::DB_TABLE)->insert([
                Role::A_NAME => $role,
                Role::A_READABLE_NAME => $readableName,
                Role::CREATED_AT => Carbon::now()->format('Y-m-d H:i:s'),
                Role::UPDATED_AT => Carbon::now()->format('Y-m-d H:i:s')
            ]);
            (new \Symfony\Component\Console\Output\ConsoleOutput)->writeLn('Regenerated role = ' . $role . ' (' . $readableName . ')');
        }
    }
}
