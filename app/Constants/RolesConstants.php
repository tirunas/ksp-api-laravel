<?php namespace App\Constants;

class RolesConstants {
    const R_ADMIN = 'admin';
    const R_SECRETORY = 'secretory';
    const R_REFEREE = 'referee';
    const R_TEAM_MANAGER = 'team_manager';
    const R_USER = 'user';

    const P_VIEW_ADMIN_PAGE = 'view.admin.page';
    const P_VIEW_ADMIN_HOME_PAGE = 'view.admin.home.page';
    const P_WRITE_COMMENTS = 'write.comments';

    const ROLES_PERMISSIONS = [
        self::R_ADMIN => [
            self::P_VIEW_ADMIN_PAGE,
            self::P_VIEW_ADMIN_HOME_PAGE
        ],
        self::R_SECRETORY => [
            self::P_WRITE_COMMENTS
        ],
        self::R_REFEREE => [
            self::P_WRITE_COMMENTS
        ],
        self::R_TEAM_MANAGER => [
            self::P_WRITE_COMMENTS
        ],
        self::R_USER => [
            self::P_WRITE_COMMENTS
        ]
    ];

    const ROLES_READABLE_NAMES = [
        self::R_ADMIN => 'Administratorius',
        self::R_SECRETORY => 'Sekretoriato darbuotojas',
        self::R_REFEREE => 'Teisejas',
        self::R_USER => 'Vartotojas',
        self::R_TEAM_MANAGER => 'Komandos vadovas'
    ];
}