<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    const PRIMARY_KEY = 'id';
    const A_NAME = 'name';
    const A_SURNAME = 'surname';
    const A_EMAIL = 'email';
    const A_PASSWORD = 'password';
    const A_REMEMBER_TOKEN = 'remember_token';
    const A_STATUS = 'status';
    const A_CITY = 'city';
    const A_BIRTHDAY = 'birthday';
    const A_REFEREE_EXP = 'referee_exp';

    const REL_ROLES = 'roles';
    const REL_TEAMS = 'teams';

    const FK_USER_ID = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::A_NAME,
        self::A_SURNAME,
        self::A_EMAIL,
        self::A_PASSWORD,
        self::A_CITY,
        self::A_BIRTHDAY,
        self::A_REFEREE_EXP
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::A_PASSWORD,
        self::A_REMEMBER_TOKEN,
    ];

//
//  GETTERS AND SETTERS
//

    public function getId(): int {
        return $this->getAttribute(self::PRIMARY_KEY);
    }
    
    public function setId(int $value)
    {
        $this->setAttribute(self::PRIMARY_KEY, $value);
    }

    public function getName(): string {
        return $this->getAttribute(self::A_NAME);
    }

    public function setName(string $value)
    {
        $this->setAttribute(self::A_NAME, $value);
    }
    
    public function getSurname(): string {
        return $this->getAttribute(self::A_SURNAME);
    }
    
    public function setSurname(string $value)
    {
        $this->setAttribute(self::A_SURNAME, $value);
    }

    public function getEmail(): string {
        return $this->getAttribute(self::A_EMAIL);
    }

    public function setEmail(string $value)
    {
        $this->setAttribute(self::A_EMAIL, $value);
    }

    public function getPassword(): string {
        return $this->getAttribute(self::A_PASSWORD);
    }

    public function setPassword(string $value)
    {
        $this->setAttribute(self::A_PASSWORD, $value);
    }
    
    public function getStatus(): ?int {
        return $this->getAttribute(self::A_STATUS);
    }
    
    public function setStatus(int $value)
    {
        $this->setAttribute(self::A_STATUS, $value);
    }
    
    public function getCity(): ?string {
        return $this->getAttribute(self::A_CITY);
    }
    
    public function setCity(string $value)
    {
        $this->setAttribute(self::A_CITY, $value);
    }
    
    public function getBirthday(): ?Carbon {
        return $this->getAttribute(self::A_BIRTHDAY);
    }
    
    public function setBirthday(Carbon $value)
    {
        $this->setAttribute(self::A_BIRTHDAY, $value);
    }
    
    public function getRefereeExp(): ?int {
        return $this->getAttribute(self::A_REFEREE_EXP);
    }
    
    public function setRefereeExp(int $value)
    {
        $this->setAttribute(self::A_REFEREE_EXP, $value);
    }

//
//    RELATIONS
//

    public function getRoles()
    {
        return $this->getRelation(self::REL_ROLES);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'users_roles', self::FK_USER_ID, Role::FK_ROLE_ID)->withTimestamps();
    }

    public function getTeams()
    {
        return $this->getRelation(self::REL_TEAMS);
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class, 'team_user', self::FK_USER_ID, Team::FK_TEAM_ID);
    }
}
