<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

const EXCEPT = ['except' => ['edit', 'create']];
Route::prefix('v1')->group( function () {

    Route::middleware(['jwt.auth'])->group(function () {
        Route::resource('users', 'UserController', EXCEPT);
        Route::resource('teams', 'TeamController', EXCEPT);
    });
    // JWT Auth starts here
    Route::prefix('auth')->group(function () {
        Route::post('register', 'AuthController@register');
        Route::post('login', 'AuthController@login');

        Route::middleware(['jwt.auth'])->group(function () {
            Route::post('logout', 'AuthController@logout');
        });

        Route::get('refresh', 'AuthController@refresh');
    });

});
